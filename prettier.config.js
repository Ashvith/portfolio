export default {
  semi: true,
  useTabs: false,
  tabWidth: 2,
  printWidth: 80,
  htmlWhitespaceSensitivity: 'strict',
  singleAttributePerLine: true,
  bracketSpacing: true,
  bracketSameLine: true,
  arrowParens: 'always',
  singleQuote: true,
  trailingComma: 'none',
  svelteStrictMode: false,
  svelteAllowShorthand: true,
  svelteIndentScriptAndStyle: true,
  plugins: ['prettier-plugin-svelte', 'prettier-plugin-tailwindcss'],
  overrides: [
    {
      files: '*.svelte',
      options: {
        parser: 'svelte'
      }
    }
  ]
};
