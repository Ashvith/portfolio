import { browser } from '$app/environment';
import { writable } from 'svelte/store';
import { defaultTheme, themeMode } from './data';

const assignValidTheme = () => {
	const theme = window.localStorage.getItem('theme');
	const themeArray = Object.keys(themeMode);

	if (themeArray.includes(theme)) {
		return theme;
	} else {
		window.localStorage.setItem('theme', defaultTheme);
		return defaultTheme;
	}
};

export const currentTheme = writable(browser ? assignValidTheme() : defaultTheme);
