import { Sun, Moon, Monitor } from 'lucide-svelte';

export const themeMode = {
	system: { idx: 0, icon: Monitor },
	light: { idx: 1, icon: Sun },
	dark: { idx: 2, icon: Moon }
};

export const defaultTheme = 'system';
