import {
  Codeberg,
  Element,
  Ruby,
  C,
  Javascript,
  Python,
  Git,
  Docker,
  Nixos,
  Postgresql,
  Mongodb,
  Redis,
  Rubyonrails,
  Express,
  Svelte,
  React,
  Tailwindcss
} from 'svelte-simples';
import { Home, ScrollText } from 'lucide-svelte';

const bio = [
  {
    title: 'Experience',
    list: [
      {
        title: 'CaRPM',
        start_date: 'Nov, 2022',
        end_date: 'Feb, 2023',
        about: 'Fullstack Intern'
      }
    ]
  },
  {
    title: 'Education',
    list: [
      {
        title: 'NMAM Institute of Technology',
        start_date: 'Aug, 2018',
        end_date: 'Sept, 2022',
        about: 'B.E. (CSE)'
      }
    ]
  }
];

const socials = [
  {
    href: 'https://codeberg.org/Ashvith/',
    component: Codeberg
  },
  {
    href: 'https://matrix.to/#/@ashvith:matrix.org',
    component: Element
  }
];

const routes = [
  {
    name: 'Home',
    href: '/',
    icon: Home
  },
  {
    name: 'Resume',
    href: 'https://codeberg.org/Ashvith/resume/raw/branch/main/resume.pdf',
    icon: ScrollText
  }
];

const skills = [
  {
    title: 'Languages',
    categories: [
      {
        name: 'Ruby',
        icon: Ruby
      },
      {
        name: 'C',
        icon: C
      },
      {
        name: 'JavaScript',
        icon: Javascript
      },
      {
        name: 'Python',
        icon: Python
      }
    ]
  },
  {
    title: 'Tools',
    categories: [
      {
        name: 'Git',
        icon: Git
      },
      {
        name: 'Docker',
        icon: Docker
      },
      {
        name: 'Nix',
        icon: Nixos
      }
    ]
  },
  {
    title: 'Databases',
    categories: [
      {
        name: 'PostgreSQL',
        icon: Postgresql
      },
      {
        name: 'MongoDB',
        icon: Mongodb
      },
      {
        name: 'Redis',
        icon: Redis
      }
    ]
  },
  {
    title: 'Libraries',
    categories: [
      {
        name: 'ExpressJS',
        icon: Express
      },
      {
        name: 'Rails',
        icon: Rubyonrails
      },
      {
        name: 'SvelteKit',
        icon: Svelte
      },
      {
        name: 'React',
        icon: React
      },
      {
        name: 'Tailwind',
        icon: Tailwindcss
      }
    ]
  }
];

const repoLink = 'https://codeberg.org/Ashvith/portfolio';

export { bio, skills, routes, socials, repoLink };
