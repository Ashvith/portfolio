export default {
  content: ['./src/**/*.{html,svelte,js}'],
  darkMode: 'class',
  theme: {
    extend: {
      fontFamily: {
        sans: 'Inter Tight'
      }
    }
  }
};
